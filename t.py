from socket import *
import tkinter as tk
import threading
BUFFER_SIZE = 1024
PORT = 12516
#Window to write username:
class LogWindow():
	def __init__(self, rt, Ct):
		self.Ct = Ct
		self.rt = rt
		self.rt.protocol("WM_DELETE_WINDOW", self.on_closing)
		self.rt.minsize(200, 120)	
		self.rt.title("Log Window")
		self.rt.rowconfigure(0, weight = 1)
		self.rt.rowconfigure(1, weight = 4)
		self.rt.columnconfigure(0, weight = 1)
		self.ent = tk.Entry(self.rt)
		self.ent.grid(column = 0, row = 0, sticky = tk.N + tk.S + tk.W + tk.E)
		self.ok_but = tk.Button(self.rt, text = "OK")
		self.ok_but.grid(column = 0, row = 1, sticky = tk.N + tk.S + tk.W + tk.E)
		self.ok_but.bind("<Button-1>", self.getName)
		self.ent.bind("<Return>", self.getName)
		
	#Method providing name to chatwindow:
	def getName(self, event):
		self.Ct.name = self.ent.get()
		self.rt.quit()
		
	#Method which kills client:
	def on_closing(self):
		self.Ct.root.destroy()
		self.rt.destroy()
		
class ChatWindow:
	def __init__(self, root):
		self.num_user = 2
		self.name = ""
		self.root = root	
		root.withdraw()
		x = tk.Tk()
		
		try:
			LogWindow(x, self)
			x.mainloop()
			x.destroy()
			root.deiconify()
			root.title("Chat")
			root.minsize(600, 400)
			if self.name == "":
				self.root.destroy()
				return
			self.root.rowconfigure(0, weight = 1)
			self.root.columnconfigure(0, weight = 2)
			self.root.columnconfigure(1, weight = 1)
			
			self.CreateWidgets()
			
			self.listener = threading.Thread(target=self.run, args=())
			self.listener.daemon = True                   
			self.listener.start()   
			
		except Exception:
			pass
	
	def CreateWidgets(self):
		self.CreateFrames()
		
		self.sendbutton = tk.Button(self.ChatF, text = "Send Message")
		self.sendbutton.bind("<Button-1>", self.Sending)
		self.sendbutton.grid(column = 0, row = 2, sticky = tk.N + tk.S + tk.W + tk.E)
		
		self.exitbutton = tk.Button(self.UsersF, text = "Exit")
		self.exitbutton.bind("<Button-1>", self.Exit)
		self.exitbutton.grid(column = 0, row = 1, sticky = tk.N + tk.S + tk.W + tk.E)
		
		self.textbox = tk.Text(self.ChatF, height = 0, width = 0)
		self.textbox.grid(column = 0, row = 1, sticky = tk.N + tk.S + tk.W + tk.E)
		self.textbox.bind("<Return>", self.Sending)
		
		self.chatbox = tk.Text(self.ChatF, height = 0, width = 0)
		self.chatbox.config(state = tk.DISABLED)
		self.chatbox.grid(column = 0, row = 0, sticky = tk.N + tk.S + tk.W + tk.E)
		
		self.users = tk.Listbox(self.UsersF, height = 0, width = 0)
		self.users.grid(column = 0, row = 0, sticky = tk.N + tk.S + tk.W + tk.E)
		self.users.insert(0, "ALL")
		self.users.insert(1, self.name)
		
	def CreateFrames(self):
		self.ChatF = tk.Frame(self.root)
		self.ChatF.grid(column = 0, row = 0, sticky = tk.N + tk.S + tk.W + tk.E)
		self.ChatF.rowconfigure(0, weight = 3)
		self.ChatF.rowconfigure(1, weight = 2)
		self.ChatF.rowconfigure(2, weight = 1)
		self.ChatF.columnconfigure(0, weight = 1)
		
		self.UsersF = tk.Frame(self.root)
		self.UsersF.grid(column = 1, row = 0, sticky = tk.N + tk.S + tk.W + tk.E)
		self.UsersF.rowconfigure(0, weight = 5)
		self.UsersF.rowconfigure(1, weight = 1)
		self.UsersF.columnconfigure(0, weight = 1)
		
	def Sending(self, event):
		text = self.textbox.get("1.0", tk.END)
		if(text == "\n"):
			return
		receiver = self.users.curselection()
		if len(receiver) == 0:
			receiver = self.users.get(0)
		else:
			receiver = self.users.get(receiver[0])
		self.textbox.delete("1.0", tk.END)
		client_socket.send(bytes(self.name + "}" + receiver + "}" + text, 'UTF-8'))
		
	def Recived(self, message):
		self.chatbox.config(state = "normal")
		self.chatbox.insert(tk.INSERT, message)
		self.chatbox.config(state = tk.DISABLED)

	def Exit(self, event):
		self.root.destroy()

	def Insert(self, name):
		self.users.insert(self.num_user, name[1:])
		self.num_user += 1

	def run(self):
		client_socket.send(bytes(self.name, 'UTF-8'))
		while 1:
			mess = client_socket.recv(BUFFER_SIZE)
			mess = mess.decode('UTF-8')
			print("dotalem mess")
			print(mess)
			if mess[0] != "#": 
				self.Recived(mess)
			else:
				print("tu bylem")
				self.Insert(mess)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(('localhost', PORT))
root = tk.Tk()
CW = ChatWindow(root)
root.mainloop()
client_socket.close()

