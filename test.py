import socket
import time
import tkinter as tk
import select
import threading
BUFFER_SIZE = 1024
PORT = 12516
SOCKETS = []
NAMES = {}
server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
server_sock.bind(('', PORT)) 
server_sock.listen(10)

class Client(threading.Thread):
	
	def __init__(self, name, clientSocket, clientAddr, server):
		threading.Thread.__init__(self)
		self.clientSocket = clientSocket;
		self.clientAddr = clientAddr;
		self.server = server
		self.name = name
	
	def run(self):
		while(1):
			message = self.clientSocket.recv(BUFFER_SIZE)
			message = message.decode('UTF-8')
			if(message == ""):
				self.clientSocket.close()
				break
			name = ""
			text = ""
			receiver = ""
			j = 0
			for i in range(0, len(message)):
				if message[i : i + 1] == "}":
					if name == "":
						name = message[0 : i]
						j = i + 1
					else:
						receiver = message[j : i]
						j = i + 1
						break
			text = message[j :]
			if receiver == "ALL":
				send_all(bytes(name + " ==> " + receiver + ":\n" + text, 'UTF-8'))
			else:
				NAMES[name].send(bytes(name + " ==> " + receiver + ":\n" + text, 'UTF-8'))
				if receiver != name:
					NAMES[receiver].send(bytes(name + " ==> " + receiver + ":\n" + text, 'UTF-8'))
			
def send_all(message):
	for sock in SOCKETS:
		sock.send(message)
try:	
	while True:
		client, addr = server_sock.accept() 
		name = client.recv(BUFFER_SIZE)
		name = name.decode('UTF-8')
		print(SOCKETS)
		for sock in SOCKETS:
			sock.send(bytes("#" + name))
		NAMES[name] = client
		SOCKETS.append(client)
		Client(name, client, addr, server_sock).start()
except:
	server_sock.close()


